import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import {LogBox} from 'react-native';

// splash screen
import SplashScreen from 'react-native-splash-screen';

// router
import {NavigationContainer} from '@react-navigation/native';
import Router from './src/router/MainRouter';
import {ref} from './src/router/navigate';

// global
import {Color} from './src/shared/global/config';

// redux
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {Persistor, Store} from './src/store/Store';

LogBox.ignoreAllLogs(true);

const darkTheme = {
  dark: true,
  colors: {
    primary: Color.primaryDark,
    background: Color.primaryDark,
    card: Color.blue,
    text: 'white',
    border: Color.paleBlue,
    notification: Color.blue,
  },
};

export default function App() {
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  return (
    <Provider store={Store}>
      <PersistGate persistor={Persistor}>
        <NavigationContainer theme={darkTheme} ref={ref}>
          <Router />
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
}
