import axios from 'axios';

// store
import {Store} from '../../store/Store';

// url
export const AUTH_URL = 'https://isengberhadiah.herokuapp.com';
export const MOVIE_URL = 'https://api.themoviedb.org/3';
export const ENDPOINT_MOVIE = '/movie';

function* doRequest(base, endpoint, options) {
  try {
    const Blacklist = ['/api/auth/login', '/api/auth/register'];
    let response;
    const {method, body, headers} = options;
    const Token = Store.getState().LoginReducer.token;
    const result = yield axios({
      url: `${base}${endpoint}`,
      method,
      data: body,
      headers: headers
        ? Token && !Blacklist.includes(endpoint)
          ? {Authorization: `Bearer ${Token}`}
          : {...headers}
        : Token && !Blacklist.includes(endpoint)
        ? {Authorization: `Bearer ${Token}`}
        : null,
    });

    if (result.status === 200) {
      response = {
        ...result,
        status: true,
      };
    }
    return response;
  } catch (error) {
    return {
      messages: "There's an error in our server, try again later",
      status: false,
    };
  }
}

export function* request(base, endpoint, method, config) {
  return yield doRequest(base, endpoint, {method, ...config});
}
