import {moderateScale} from 'react-native-size-matters';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

export const Color = {
  lightBlue: '#F1FCFD',
  paleBlue: '#C7EEFF',
  blue: '#4D6DE3',
  primaryDark: '#393737',
};

export const Size = {
  ms6: moderateScale(6),
  ms10: moderateScale(10),
  ms12: moderateScale(12),
  ms14: moderateScale(14),
  ms16: moderateScale(16),
  ms20: moderateScale(20),
  ms22: moderateScale(22),
  ms24: moderateScale(24),
  ms28: moderateScale(28),
  ms32: moderateScale(32),
  ms40: moderateScale(40),
  ms80: moderateScale(80),
  ms100: moderateScale(100),
  ms200: moderateScale(200),

  wp4: widthPercentageToDP(4),
  wp80: widthPercentageToDP(80),
  wp92: widthPercentageToDP(92),
  wp100: widthPercentageToDP(100),

  hp1: heightPercentageToDP(1),
  hp2: heightPercentageToDP(2),
  hp3: heightPercentageToDP(3),
  hp4: heightPercentageToDP(4),
  hp10: heightPercentageToDP(10),
  hp20: heightPercentageToDP(20),
  hp48: heightPercentageToDP(48),
  hp72: heightPercentageToDP(72),
  hp100: heightPercentageToDP(100),
};
