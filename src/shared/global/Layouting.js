import {StyleSheet} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {Size} from './config';

export const Layouting = (type = 'center', num = 1, color = 'black') => {
  return StyleSheet.create({
    flex: {
      flex: num,
    },
    flexRow: {
      flexDirection: 'row',
    },
    centered: {
      justifyContent: 'center',
      alignItems: type,
    },
    spaceBetween: {
      justifyContent: 'space-between',
      alignItems: type,
    },
    spaceAround: {
      justifyContent: 'space-around',
      alignItems: type,
    },
    spaceEvenly: {
      justifyContent: 'space-evenly',
      alignItems: type,
    },
    flexStart: {
      justifyContent: 'flex-start',
      alignItems: type,
    },
    flexEnd: {
      justifyContent: 'flex-end',
      alignItems: type,
    },
    centerAlign: {
      alignItems: 'center',
    },
    shadow: {
      shadowColor: color,
      shadowOffset: {width: 0, height: Size.ms20},
      shadowOpacity: moderateScale(0.51),
      shadowRadius: moderateScale(13.16),
      elevation: Size.ms20,
    },
  });
};
