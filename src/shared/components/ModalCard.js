import React, {useEffect, useState} from 'react';
import {ScrollView, StyleSheet, TouchableOpacity, View} from 'react-native';
import {Input, Rating} from 'react-native-elements';

// component
import {DarkButton} from './DarkButton';
import Roboto from './Roboto';

// global
import {Color, Size} from '../global/config';
import {Layouting} from '../global/Layouting';

export const ModalCard = ({
  onPress,
  startingValue,
  onFinishRating,
  title,
  review,
  onChangeTitle,
  onChangeReview,
}) => {
  const [focused, setFocused] = useState(true);

  useEffect(() => {
    handleFocus();
    handleBlur();
  }, []);

  const handleFocus = () => {
    setFocused(true);
  };

  const handleBlur = () => {
    setFocused(false);
  };

  return (
    <View style={[Layouting().flex, Layouting().centered, styles.container]}>
      <View style={[Layouting().shadow, Layouting().spaceAround, styles.modal]}>
        <Roboto
          title="How do you think about this movie?"
          color="black"
          size={Size.ms16}
          type="Bold"
        />

        <Rating
          type="custom"
          ratingCount={10}
          showRating
          startingValue={startingValue}
          fractions={1}
          onFinishRating={onFinishRating}
          ratingTextColor="#0b3ae8"
          imageSize={Size.ms28}
          tintColor={Color.paleBlue}
          ratingColor={Color.blue}
          ratingBackgroundColor="lightsteelblue"
          style={styles.rating}
        />

        <ScrollView
          keyboardDismissMode="interactive"
          keyboardShouldPersistTaps="handled">
          <Input
            onFocus={handleFocus}
            onBlur={handleBlur}
            autoCapitalize="sentences"
            placeholder={
              title ? title : 'Write a headline for your review here'
            }
            placeholderTextColor={title ? 'black' : 'gray'}
            backgroundColor="white"
            inputStyle={styles.titleInput}
            inputContainerStyle={styles.inputContainer}
            style={styles.inputBox}
            multiline={true}
            value={title}
            onChangeText={onChangeTitle}
          />

          <Input
            onFocus={handleFocus}
            onBlur={handleBlur}
            autoCapitalize="sentences"
            placeholder={review ? review : 'Write your review here'}
            placeholderTextColor={review ? 'black' : 'gray'}
            backgroundColor="white"
            inputStyle={styles.reviewInput}
            inputContainerStyle={styles.inputContainer}
            style={[styles.inputBox, styles.reviewBox]}
            multiline={true}
            value={review}
            onChangeText={onChangeReview}
          />
        </ScrollView>

        <TouchableOpacity style={[Layouting().centered, styles.icons]}>
          <DarkButton title="Submit" onPress={onPress} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: Size.wp4,
  },
  modal: {
    width: Size.wp92,
    backgroundColor: Color.paleBlue,
    paddingHorizontal: Size.wp4,
    paddingVertical: Size.hp4,
    borderRadius: Size.ms20,
  },
  rating: {
    marginVertical: Size.hp2,
  },
  inputContainer: {
    borderBottomWidth: 0,
    width: Size.wp80,
  },
  inputBox: {
    borderRadius: Size.ms10,
  },
  titleInput: {
    fontSize: Size.ms14,
    fontFamily: 'Roboto-Bold',
  },
  reviewInput: {
    fontSize: Size.ms12,
    fontFamily: 'Roboto-Regular',
  },
  reviewBox: {
    height: Size.ms100,
  },
});
