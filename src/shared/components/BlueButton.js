import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {moderateScale} from 'react-native-size-matters';

// component
import Roboto from './Roboto';

// global
import {Color, Size} from '../global/config';
import {Layouting} from '../global/Layouting';

export const BlueButton = ({onPress, title, disabled}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.8}
      disabled={disabled}
      style={[
        Layouting().centered,
        styles.button,
        {backgroundColor: disabled ? Color.paleBlue : Color.blue},
      ]}>
      <Roboto
        title={title}
        color={disabled ? Color.primaryDark : 'white'}
        size={Size.ms16}
        type="Bold"
        style={styles.text}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    width: moderateScale(124),
    height: Size.ms40,
    borderRadius: Size.ms100,
    margin: Size.ms20,
  },
  text: {
    textTransform: 'uppercase',
  },
});
