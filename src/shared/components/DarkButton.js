import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';

// component
import Roboto from './Roboto';

// global
import {Color, Size} from '../global/config';
import {Layouting} from '../global/Layouting';

export const DarkButton = ({onPress, title}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.8}
      style={[Layouting().centered, styles.button]}>
      <Roboto
        title={title}
        color="white"
        size={Size.ms16}
        type="Medium"
        style={styles.text}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    width: Size.ms100,
    height: Size.ms32,
    borderRadius: Size.ms10,
    backgroundColor: Color.primaryDark,
  },
});
