import React from 'react';
import {Text} from 'react-native';
import {Size} from '../../shared/global/config';

export default function Roboto({
  title,
  onPress,
  color = 'white',
  size = Size.ms12,
  type = 'Regular',
  numberOfLines,
  onTextLayout,
  style,
}) {
  return (
    <Text
      onPress={onPress}
      numberOfLines={numberOfLines}
      onTextLayout={onTextLayout}
      style={{
        color,
        fontSize: size,
        fontFamily: `Roboto-${type}`,
        ...style,
      }}>
      {title}
    </Text>
  );
}
