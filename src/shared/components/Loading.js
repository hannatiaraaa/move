import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import {Color, Size} from '../global/config';
import {Layouting} from '../global/Layouting';

export default function Loading() {
  return (
    <View style={[Layouting().centered, Layouting().flex]}>
      <ActivityIndicator size={Size.ms32} color={Color.paleBlue} />
    </View>
  );
}
