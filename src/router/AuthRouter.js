import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// screen
import Login from '../features/Login/Login';
import Register from '../features/Register/Register';

const AuthStack = createStackNavigator();

export function AuthRouter() {
  return (
    <AuthStack.Navigator initialRouteName="Login">
      <AuthStack.Screen
        name="Login"
        component={Login}
        options={{headerShown: false}}
      />
      <AuthStack.Screen
        name="Register"
        component={Register}
        options={{headerShown: false}}
      />
    </AuthStack.Navigator>
  );
}
