import {createRef} from 'react';

export const ref = createRef();

export function navigate(name, params) {
  ref.current?.navigate(name, params);
}
