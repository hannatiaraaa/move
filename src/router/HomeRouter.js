import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// screen
import Home from '../features/Home/Home';
import AllReviews from '../features/Home/AllReviews';

// component
import Roboto from '../shared/components/Roboto';

// global
import {Color, Size} from '../shared/global/config';

const HomeStack = createStackNavigator();

export function HomeRouter() {
  return (
    <HomeStack.Navigator initialRouteName="Home">
      <HomeStack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <HomeStack.Screen
        name="AllReviews"
        component={AllReviews}
        options={{
          headerTitle: (
            <Roboto
              title="All Reviews"
              size={Size.ms20}
              color={Color.lightBlue}
              type="Medium"
            />
          ),
          headerStyle: {
            backgroundColor: Color.blue,
            height: Size.hp10,
          },
        }}
      />
    </HomeStack.Navigator>
  );
}
