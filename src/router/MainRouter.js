import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// router
import {AuthRouter} from './AuthRouter';
import {TabRouter} from './TabRouter';

// redux
import {connect} from 'react-redux';

const Stack = createStackNavigator();

function MainRouter(props) {
  return (
    <Stack.Navigator
      initialRouteName={props.token === '' ? 'Auth' : 'LoggedIn'}>
      <Stack.Screen
        name="Auth"
        component={AuthRouter}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="LoggedIn"
        component={TabRouter}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}

const mapStateToProps = (state) => ({
  token: state.LoginReducer.token,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(MainRouter);
