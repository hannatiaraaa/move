import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// router
import {HomeRouter} from './HomeRouter';
import {ProfileRouter} from './ProfileRouter';

// screen
import Review from '../features/Review/UserReviews';

// global
import {Color, Size} from '../shared/global/config';

// icon
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';

const BottomTab = createBottomTabNavigator();

export function TabRouter() {
  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      shifting={true}
      activeColor={Color.primaryDark}
      inactiveColor="rgba(77,109,227, 0.5)"
      tabBarOptions={{
        showLabel: false,
        tabStyle: {backgroundColor: Color.lightBlue},
        style: {backgroundColor: Color.lightBlue},
      }}>
      <BottomTab.Screen
        name="Review"
        component={Review}
        options={{
          tabBarIcon: ({color, focused}) => (
            <FontAwesome
              name={focused ? 'commenting' : 'comment-o'}
              color={color}
              size={focused ? Size.ms24 : Size.ms20}
            />
          ),
        }}
      />
      <BottomTab.Screen
        name="Home"
        component={HomeRouter}
        options={{
          tabBarIcon: ({color, focused}) => (
            <Ionicons
              name={focused ? 'ios-home' : 'ios-home-outline'}
              color={color}
              size={focused ? Size.ms24 : Size.ms20}
            />
          ),
        }}
      />
      <BottomTab.Screen
        name="Profile"
        component={ProfileRouter}
        options={{
          tabBarIcon: ({color, focused}) => (
            <FontAwesome
              name={focused ? 'user-circle' : 'user-circle-o'}
              color={color}
              size={focused ? Size.ms24 : Size.ms20}
            />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
}
