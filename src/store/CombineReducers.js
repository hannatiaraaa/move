import {combineReducers} from 'redux';
import {GlobalReducer} from './GlobalReducer';
import {HomeReducer} from '../features/Home/redux/reducer';
import {LoginReducer} from '../features/Login/redux/reducer';
import {RegisterReducer} from '../features/Register/redux/reducer';
import {ProfileReducer} from '../features/Profile/redux/reducer';

export const CombineReducers = combineReducers({
  GlobalReducer,
  HomeReducer,
  LoginReducer,
  RegisterReducer,
  ProfileReducer,
});
