import {all} from 'redux-saga/effects';
import {HomeSaga} from '../features/Home/redux/saga';
import {LoginSaga} from '../features/Login/redux/saga';
import {RegisterSaga} from '../features/Register/redux/saga';
import {ProfileSaga} from '../features/Profile/redux/saga';

export function* SagaWatcher() {
  yield all([HomeSaga(), LoginSaga(), RegisterSaga(), ProfileSaga()]);
}
