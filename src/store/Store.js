import {applyMiddleware, createStore} from 'redux';
import {persistReducer, persistStore} from 'redux-persist';

// storage
import AsyncStorage from '@react-native-async-storage/async-storage';

// reducer
import {CombineReducers} from './CombineReducers';

// middleware
import logger from 'redux-logger';
import createSagaMiddleWare from 'redux-saga';

// saga
import {SagaWatcher} from './SagaWatcher';

const persistConfig = {
  key: 'movE',
  storage: AsyncStorage,
};

const SagaMiddleWare = createSagaMiddleWare();

const AllMiddleWare = applyMiddleware(SagaMiddleWare, logger);

const PersistedReducer = persistReducer(persistConfig, CombineReducers);

export const Store = createStore(PersistedReducer, AllMiddleWare);

export const Persistor = persistStore(Store);

SagaMiddleWare.run(SagaWatcher);
