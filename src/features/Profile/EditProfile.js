import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {TouchableHighlight, ScrollView, View, StyleSheet} from 'react-native';
import FastImage from 'react-native-fast-image';
import {moderateScale} from 'react-native-size-matters';

// component
import {HeaderEditProfile} from './components/Header';
import {EditInput} from './components/EditInput';

// global
import {Layouting} from '../../shared/global/Layouting';
import {Color, Size} from '../../shared/global/config';

// icon
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';

// redux
import {connect} from 'react-redux';
import {getProfile} from './redux/action';

function EditProfile(props) {
  useEffect(() => {
    props.getProfile(props.token);
  }, []);

  const user = props.userData;
  const [name, setName] = useState(user.name);
  const [username, setUsername] = useState(user.username);
  const [password, setPassword] = useState(user.password);
  const [passwordError, setPasswordError] = useState('');

  const validatePassword = (text) => {
    setPassword(text);
    if (password.length >= 6) {
      setPasswordError(null);
    } else {
      setPasswordError('Passwords must be at least 6 characters long');
    }
  };

  const navigatToProfile = () => {
    props.navigation.navigate('Profile');
  };

  return (
    <>
      <HeaderEditProfile
        onPressCancel={navigatToProfile}
        onPressSave={navigatToProfile}
      />
      <SafeAreaView
        style={[Layouting().flex, Layouting().spaceEvenly, styles.container]}>
        <ScrollView contentContainerStyle={[Layouting().spaceAround]}>
          <View style={[Layouting().centered]}>
            {user.profileImage === undefined || user.profileImage === null ? (
              <View>
                <View
                  style={[
                    Layouting().centered,
                    Layouting().shadow,
                    styles.avatar,
                    styles.imageIcon,
                  ]}>
                  <FontAwesome
                    name="user-circle"
                    color={Color.paleBlue}
                    size={Size.ms100}
                  />
                </View>
                <View style={[styles.iconEdit, Layouting('flex-end').flexEnd]}>
                  <TouchableHighlight
                    underlayColor={Color.primaryDark}
                    style={[Layouting().centered, styles.icons]}>
                    <Entypo name="pencil" size={Size.ms16} color={Color.blue} />
                  </TouchableHighlight>
                </View>
              </View>
            ) : (
              <View>
                <FastImage
                  source={{uri: `${user.profileImage}`}}
                  resizeMode="contain"
                  style={[
                    {
                      ...StyleSheet.absoluteFillObject,
                    },
                    Layouting().shadow,
                    styles.avatar,
                    styles.image,
                  ]}
                />
                <View style={[styles.iconEdit, Layouting('flex-end').flexEnd]}>
                  <TouchableHighlight
                    underlayColor={Color.primaryDark}
                    style={[Layouting().centered, styles.icons]}>
                    <Entypo name="pencil" size={Size.ms16} color={Color.blue} />
                  </TouchableHighlight>
                </View>
              </View>
            )}
          </View>

          <EditInput
            title="Full Name"
            content={name}
            onChangeText={(text) => setName(text)}
          />
          <EditInput
            title="Username"
            content={username}
            onChangeText={(text) => setUsername(text)}
            error={username ? null : 'Username is required'}
          />
          <EditInput
            title="Email Address"
            content={user.email}
            disabled={true}
          />
          <EditInput
            title="Password"
            onChangeText={(text) => validatePassword(text)}
            secureTextEntry={true}
            error={passwordError}
          />
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const mapStateToProps = (state) => ({
  token: state.LoginReducer.token,
  userData: state.ProfileReducer.userData,
});

const mapDispatchToProps = {
  getProfile,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
  },
  avatar: {
    borderRadius: moderateScale(50),
    width: Size.ms100,
    height: Size.ms100,
    marginBottom: Size.hp4,
  },
  imageIcon: {
    backgroundColor: Color.blue,
    position: 'absolute',
    elevation: 3,
  },
  image: {
    backgroundColor: Color.paleBlue,
    elevation: 3,
  },
  iconEdit: {
    width: Size.ms100,
    height: Size.ms100,
    position: 'relative',
    zIndex: 2,
    elevation: 3,
    textAlign: 'center',
  },
  icons: {
    borderRadius: moderateScale(44),
    width: Size.ms22,
    height: Size.ms22,
    backgroundColor: 'yellow',
    marginRight: Size.ms10,
  },
});
