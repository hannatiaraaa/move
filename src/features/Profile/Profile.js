import React, {useEffect} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {ScrollView, View, StyleSheet} from 'react-native';
import FastImage from 'react-native-fast-image';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';

// component
import {HeaderProfile} from './components/Header';
import {DataProfile} from './components/DataProfile';
import {BlueButton} from '../../shared/components/BlueButton';

// global
import {Layouting} from '../../shared/global/Layouting';
import {Color, Size} from '../../shared/global/config';

// icon
import FontAwesome from 'react-native-vector-icons/FontAwesome';

// redux
import {connect} from 'react-redux';
import {getProfile} from './redux/action';
import {actionLogout} from '../Login/redux/action';

function Profile(props) {
  useEffect(() => {
    props.getProfile(props.token);
  }, []);

  const user = props.userData;

  const navigateToEdit = () => {
    props.navigation.navigate('EditProfile');
  };

  return (
    <>
      <HeaderProfile onPressEdit={navigateToEdit} />
      <SafeAreaView
        style={[Layouting().flex, Layouting().spaceEvenly, styles.container]}>
        <ScrollView
          contentContainerStyle={[
            Layouting().spaceEvenly,
            styles.contentContainer,
          ]}>
          <View style={[Layouting().spaceAround]}>
            <View style={[Layouting().centered]}>
              {user.profileImage === undefined || user.profileImage === null ? (
                <View
                  style={[
                    Layouting().centered,
                    Layouting().shadow,
                    styles.avatar,
                    styles.imageIcon,
                  ]}>
                  <FontAwesome
                    name="user-circle"
                    color={Color.paleBlue}
                    size={Size.ms100}
                  />
                </View>
              ) : (
                <FastImage
                  source={{uri: `${user.profileImage}`}}
                  resizeMode="contain"
                  style={[
                    {
                      ...StyleSheet.absoluteFillObject,
                    },
                    Layouting().shadow,
                    styles.avatar,
                    styles.image,
                  ]}
                />
              )}
            </View>

            {user.name === '' ||
            user.name === undefined ||
            user.name === null ? null : (
              <DataProfile title="Full Name" content={user.name} />
            )}
            <DataProfile title="Username" content={user.username} />
            <DataProfile title="email" content={user.email} />
          </View>

          <BlueButton onPress={() => props.actionLogout()} title="sign out" />
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const mapStateToProps = (state) => ({
  token: state.LoginReducer.token,
  userData: state.ProfileReducer.userData,
});

const mapDispatchToProps = {
  getProfile,
  actionLogout,
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
  },
  contentContainer: {
    width: Size.wp92,
    height: heightPercentageToDP(78),
  },
  avatar: {
    borderRadius: moderateScale(50),
    width: Size.ms100,
    height: Size.ms100,
    position: 'relative',
    marginBottom: Size.hp4,
  },
  imageIcon: {
    backgroundColor: Color.blue,
  },
  image: {
    backgroundColor: Color.paleBlue,
  },
});
