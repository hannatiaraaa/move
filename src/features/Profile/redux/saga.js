import {all, call, put, takeLatest} from 'redux-saga/effects';

// api
import {request, AUTH_URL} from '../../../shared/utils/api';
import {USER_URL} from '../endPoint';

// router
import {navigate} from '../../../router/navigate';

// action
import {GET_PROFILE, setProfile} from './action';
import {LOG_OUT} from '../../Login/redux/action';

function* getProfileSaga({token}) {
  const res = yield call(request, `${AUTH_URL}`, `${USER_URL}`, 'GET', {
    headers: {token},
  });

  if (res.status) {
    yield put(setProfile(res.data.user));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* LogoutSaga() {
  yield navigate({
    name: 'Auth',
    params: {},
  });
}

export function* ProfileSaga() {
  yield all([
    takeLatest(GET_PROFILE, getProfileSaga),
    takeLatest(LOG_OUT, LogoutSaga),
  ]);
}
