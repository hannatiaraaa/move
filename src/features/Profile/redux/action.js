export const GET_PROFILE = 'GET_PROFILE';
export const SET_PROFILE = 'SET_PROFILE';

// GET
export const getProfile = (token) => {
  return {
    type: GET_PROFILE,
    token,
  };
};

// SET
export const setProfile = (payload) => {
  return {
    type: SET_PROFILE,
    payload,
  };
};
