import {SET_PROFILE} from './action';

const initialState = {
  userData: {},
};

export const ProfileReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PROFILE:
      return {
        ...state,
        userData: action.payload,
      };

    default:
      return state;
  }
};
