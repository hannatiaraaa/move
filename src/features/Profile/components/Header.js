import React from 'react';
import {Header} from 'react-native-elements';

// component
import Roboto from '../../../shared/components/Roboto';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// icon
import Entypo from 'react-native-vector-icons/Entypo';
import Fontisto from 'react-native-vector-icons/Fontisto';

export const HeaderProfile = ({onPressEdit}) => {
  return (
    <Header
      containerStyle={[
        Layouting().spaceAround,
        {
          backgroundColor: Color.blue,
          height: Size.hp10,
          paddingHorizontal: Size.wp4,
        },
      ]}
      centerComponent={
        <Roboto
          title="Your Profile"
          size={Size.ms20}
          color={Color.lightBlue}
          type="MediumItalic"
        />
      }
      rightComponent={
        <Entypo
          onPress={onPressEdit}
          name="pencil"
          size={Size.ms24}
          color="yellow"
        />
      }
    />
  );
};

export const HeaderEditProfile = ({onPressCancel, onPressSave}) => {
  return (
    <Header
      containerStyle={[
        Layouting('flex-end').spaceAround,
        {
          backgroundColor: Color.blue,
          height: Size.hp10,
          paddingHorizontal: Size.wp4,
        },
      ]}
      leftComponent={
        <Fontisto
          onPress={onPressCancel}
          name="close-a"
          size={Size.ms20}
          color="red"
        />
      }
      centerComponent={
        <Roboto
          title="Edit Profile"
          size={Size.ms20}
          color={Color.lightBlue}
          type="Medium"
        />
      }
      rightComponent={
        <Entypo
          onPress={onPressSave}
          name="check"
          size={Size.ms24}
          color="yellow"
        />
      }
    />
  );
};
