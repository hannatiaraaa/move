import React, {useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Input} from 'react-native-elements';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

export const EditInput = ({
  title,
  content,
  onChangeText,
  disabled = false,
  secureTextEntry = false,
  error,
}) => {
  const [focused, setFocused] = useState(true);

  useEffect(() => {
    handleFocus();
    handleBlur();
  }, []);

  const handleFocus = () => {
    setFocused(true);
  };

  const handleBlur = () => {
    setFocused(false);
  };

  return (
    <View style={[Layouting().spaceAround, styles.container]}>
      <Input
        secureTextEntry={secureTextEntry}
        onFocus={handleFocus}
        onBlur={handleBlur}
        label={title}
        labelStyle={styles.title}
        placeholder={content ? content : title}
        disabled={disabled}
        value={content}
        onChangeText={disabled ? content : onChangeText}
        inputContainerStyle={
          focused ? styles.borderInput : styles.borderNonInput
        }
        inputStyle={styles.textInput}
        errorStyle={styles.textError}
        errorMessage={error}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: Size.wp92,
    marginVertical: Size.hp2,
  },
  title: {
    color: Color.paleBlue,
    textTransform: 'uppercase',
    fontFamily: 'Roboto-MediumLight',
    fontSize: Size.ms12,
  },
  borderInput: {
    borderColor: Color.blue,
  },
  borderNonInput: {
    borderColor: Color.paleBlue,
  },
  textInput: {
    color: 'white',
    fontSize: Size.ms16,
  },
  textError: {
    color: 'red',
  },
});
