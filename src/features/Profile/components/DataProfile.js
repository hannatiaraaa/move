import React from 'react';
import {View, StyleSheet} from 'react-native';

// component
import Roboto from '../../../shared/components/Roboto';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

export const DataProfile = ({title, content}) => {
  return (
    <View style={[Layouting().spaceAround, styles.container]}>
      <Roboto
        title={title}
        size={Size.ms16}
        color={Color.paleBlue}
        style={styles.title}
      />
      <Roboto
        title={content}
        size={Size.ms24}
        type="Bold"
        style={styles.content}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: Size.hp2,
  },
  title: {
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  content: {
    textAlign: 'center',
  },
});
