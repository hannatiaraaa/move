import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {BackHandler, ScrollView, StyleSheet} from 'react-native';
import FastImage from 'react-native-fast-image';
import {Input} from 'react-native-elements';

// component
import Loading from '../../shared/components/Loading';
import {BlueButton} from '../../shared/components/BlueButton';
import Roboto from '../../shared/components/Roboto';

// global
import {Layouting} from '../../shared/global/Layouting';
import {Color, Size} from '../../shared/global/config';

// icon
import Zocial from 'react-native-vector-icons/Zocial';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

// redux
import {connect} from 'react-redux';
import {actionLogin} from './redux/action';
import {setLoading} from '../../store/GlobalAction';

function Login(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [focused, setFocused] = useState(true);

  useEffect(() => {
    handleFocus();
    handleBlur();
  }, []);

  BackHandler.addEventListener('hardwareBackPress', function () {
    BackHandler.exitApp();
  });

  const handleFocus = () => {
    setFocused(true);
  };

  const handleBlur = () => {
    setFocused(false);
  };

  const validateEmail = (text) => {
    setEmail(text);
  };

  const validatePassword = (text) => {
    setPassword(text);
  };

  const LoginProcess = () => {
    props.actionLogin({
      email,
      password,
    });
  };

  const navigateToRegister = () => {
    props.navigation.navigate('Register');
  };

  return (
    <>
      {props.isLoading ? (
        <Loading />
      ) : (
        <SafeAreaView style={[Layouting().flex, Layouting().flexEnd]}>
          <ScrollView
            contentContainerStyle={[
              Layouting().flex,
              Layouting().centered,
              styles.container,
            ]}>
            <FastImage
              source={require('../../assets/images/logo.png')}
              style={styles.logo}
            />

            <Input
              onFocus={handleFocus}
              onBlur={handleBlur}
              autoCapitalize="none"
              placeholder="Email Address"
              leftIcon={
                <Zocial name="email" size={Size.ms20} color={Color.paleBlue} />
              }
              onChangeText={(text) => {
                validateEmail(text);
              }}
              errorStyle={styles.textError}
              errorMessage={
                focused && !email.trim()
                  ? 'Please enter your email address'
                  : null
              }
              inputContainerStyle={
                focused ? styles.borderInput : styles.borderNonInput
              }
              inputStyle={styles.textInput}
            />
            <Input
              onFocus={handleFocus}
              onBlur={handleBlur}
              placeholder="Password"
              leftIcon={
                <MaterialIcons
                  name="lock"
                  size={Size.ms20}
                  color={Color.paleBlue}
                />
              }
              secureTextEntry={true}
              onChangeText={(text) => {
                validatePassword(text);
              }}
              errorStyle={styles.textError}
              errorMessage={
                focused && !password.trim()
                  ? 'Please enter your password'
                  : null
              }
              inputContainerStyle={
                focused ? styles.borderInput : styles.borderNonInput
              }
              inputStyle={styles.textInput}
            />

            <Roboto
              title="Forgot your password?"
              style={styles.forgotPassword}
            />
            <BlueButton
              onPress={LoginProcess}
              title="sign in"
              disabled={email && password ? false : true}
            />

            <Roboto
              title={
                <>
                  Don't have an account?{' '}
                  <Roboto
                    title="Sign Up"
                    onPress={navigateToRegister}
                    type="Medium"
                  />
                </>
              }
            />
          </ScrollView>
        </SafeAreaView>
      )}
    </>
  );
}

const mapStateToProps = (state) => ({
  isLoading: state.GlobalReducer.isLoading,
});

const mapDispatchToProps = {
  actionLogin,
  setLoading,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  container: {
    width: Size.wp92,
    paddingHorizontal: Size.wp4,
  },
  logo: {
    width: Size.ms200,
    height: Size.ms200,
    margin: Size.ms12,
  },
  textInput: {
    color: 'white',
    fontSize: Size.ms16,
  },
  textError: {
    color: 'red',
  },
  borderInput: {
    borderColor: Color.blue,
  },
  borderNonInput: {
    borderColor: Color.paleBlue,
  },
  forgotPassword: {
    width: Size.wp92,
    paddingHorizontal: Size.wp4,
    textAlign: 'right',
  },
});
