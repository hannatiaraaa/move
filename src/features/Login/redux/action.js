export const LOG_IN = 'LOG_IN';
export const SET_TOKEN = 'SET_TOKEN';
export const LOG_OUT = 'LOG_OUT';

export const actionLogin = (payload) => {
  return {
    type: LOG_IN,
    email: payload.email,
    password: payload.password,
  };
};

export const setToken = (token) => {
  return {
    type: SET_TOKEN,
    token,
  };
};

export const actionLogout = () => {
  return {
    type: LOG_OUT,
  };
};
