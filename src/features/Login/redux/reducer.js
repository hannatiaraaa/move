import {LOG_IN, SET_TOKEN, LOG_OUT} from './action';

const initialState = {
  email: '',
  token: '',
  isLoggged: false,
};

export const LoginReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOG_IN:
      return {
        ...state,
        email: action.email,
      };

    case SET_TOKEN:
      return {
        ...state,
        token: action.token,
        isLoggged: true,
      };

    case LOG_OUT:
      return {
        ...state,
        token: '',
        isLogged: false,
      };

    default:
      return state;
  }
};
