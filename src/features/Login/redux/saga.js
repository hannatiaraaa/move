import {Alert} from 'react-native';
import {all, call, put, takeLatest} from 'redux-saga/effects';

// api
import {request, AUTH_URL} from '../../../shared/utils/api';
import {LOGIN_URL} from '../endPoint';

// router
import {navigate} from '../../../router/navigate';

// action
import {setLoading} from '../../../store/GlobalAction';
import {LOG_IN, setToken} from './action';

function* actionLoginSaga({email, password}) {
  try {
    yield put(setLoading(true));
    const res = yield call(request, `${AUTH_URL}`, `${LOGIN_URL}`, 'POST', {
      body: {
        email,
        password,
      },
    });

    if (res.status) {
      yield put(setToken(res.data.token));
      yield navigate({
        name: 'LoggedIn',
        params: {},
      });
    } else {
      yield Alert.alert('Incorrect email or password', undefined, [
        {text: 'Try Again', style: 'cancel'},
      ]);
    }
  } finally {
    yield put(setLoading(false));
  }
}

export function* LoginSaga() {
  yield all([takeLatest(LOG_IN, actionLoginSaga)]);
}
