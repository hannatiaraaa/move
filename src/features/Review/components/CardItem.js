import React, {useCallback, useEffect, useState} from 'react';
import {View, StyleSheet, TouchableHighlight, Alert} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import FastImage from 'react-native-fast-image';
import {AirbnbRating, Overlay} from 'react-native-elements';

// component
import Roboto from '../../../shared/components/Roboto';
import {ModalCard} from '../../../shared/components/ModalCard';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// icon
import Entypo from 'react-native-vector-icons/Entypo';

export default function CardItem() {
  const [showMoreButton, setShowMoreButton] = useState(false);
  const [textShown, setTextShown] = useState(false);
  const [numLines, setNumLines] = useState(undefined);
  const [modalVisible, setModalVisible] = useState(false);
  const [rating, setRating] = useState(7);
  const [reviewTitle, setReviewTitle] = useState('Review Headliness!!!');
  const [review, setReview] = useState(
    "It isn't as easy as saying 'Wonder Woman 1984' is a good or bad movie. The pieces are there, and there are moments I adore, but it does come across as a bit of a mess, even though the action sequences are breathtaking. If you're a fan of the original film, you'll be more willing to take the ride, but for those more indifferent, it may be a bit of a blander sit. If you can and are planning to watch it, the theatrical experience is the way to go - there is nothing like seeing these stunning sets, fun action scenes and hearing Zimmer's jaw-dropping score like on the big screen.\r\n- Chris dos Santos\r\n\r\nRead Chris' full article...\r\nhttps://www.maketheswitch.com.au/article/review-wonder-woman-1984-a-new-era-of-wonder-occasionally",
  );

  useEffect(() => {
    setNumLines(textShown ? undefined : 4);
  }, [textShown]);

  const toggleTextShown = () => {
    setTextShown(!textShown);
  };

  const onTextLayout = useCallback(
    (e) => {
      if (e.nativeEvent.lines.length > 4 && !textShown) {
        setShowMoreButton(true);
        setNumLines(4);
      }
    },
    [textShown],
  );

  const poster = '/8UlWHLMpgZm9bx6QYh0NFoq67TZ.jpg';

  const title = 'Wonder Woman hehehehehehe';

  const year = 2020;

  const dateReview = 'December 18, 2020';

  const RenderPoster = () => {
    return (
      <FastImage
        source={{
          uri: `https://image.tmdb.org/t/p/w500${poster}`,
        }}
        resizeMode="contain"
        style={[
          {
            ...StyleSheet.absoluteFillObject,
          },
          Layouting().shadow,
          Layouting(undefined, 1).flex,
          styles.poster,
        ]}
      />
    );
  };

  const RenderTitle = () => {
    return (
      <View style={styles.contentDetails}>
        <Roboto
          title={
            <>
              {title}
              {' ('}
              {year}
              {')'}
            </>
          }
          size={Size.ms16}
          color="black"
          type="Bold"
          numberOfLines={3}
        />
      </View>
    );
  };

  const RenderDateReview = () => {
    return (
      <View style={styles.contentDetails}>
        <Roboto title={<>Reviewed {dateReview}</>} color="black" />
      </View>
    );
  };

  const RenderRating = () => {
    return (
      <View
        style={[
          Layouting().flexRow,
          Layouting().centered,
          styles.contentDetails,
        ]}>
        <AirbnbRating
          count={1}
          showRating={false}
          isDisabled={true}
          defaultRating={rating}
          size={Size.ms16}
          fractions={1}
        />
        <Roboto
          title={
            <>
              <Roboto title={rating} color="black" type="Bold" /> / 10
            </>
          }
          color="black"
          size={Size.ms10}
        />
      </View>
    );
  };

  const RenderModal = () => {
    return (
      <Overlay
        isVisible={modalVisible}
        animationType="slide"
        fullScreen={true}
        overlayStyle={styles.modal}
        onRequestClose={() => {
          Alert.alert('Your edited review should be submitted');
        }}>
        <ModalCard
          onPress={() => {
            setModalVisible(!modalVisible);
          }}
          startingValue={rating}
          title={reviewTitle}
          review={review}
          onFinishRating={(text) => setRating(text)}
          onChangeTitle={(text) => setReviewTitle(text)}
          onChangeReview={(text) => setReview(text)}
        />
      </Overlay>
    );
  };

  const RenderIcons = () => {
    return (
      <View style={[Layouting().flexRow]}>
        <TouchableHighlight
          onPress={() => {
            setModalVisible(true);
          }}
          underlayColor={Color.primaryDark}
          style={[Layouting().centered, styles.icons]}>
          <Entypo name="pencil" size={Size.ms10} color={Color.lightBlue} />
        </TouchableHighlight>
        <TouchableHighlight style={[Layouting().centered, styles.icons]}>
          <Entypo name="trash" size={Size.ms10} color={Color.lightBlue} />
        </TouchableHighlight>
      </View>
    );
  };

  const RenderReview = () => {
    return (
      <View style={styles.content}>
        <Roboto
          title={reviewTitle}
          size={Size.ms14}
          type="Bold"
          color="black"
        />
        <Roboto
          title={review}
          color="black"
          onTextLayout={onTextLayout}
          numberOfLines={numLines}
        />
        {showMoreButton ? (
          <Roboto
            title={textShown ? 'Read less...' : 'Read more...'}
            color={Color.blue}
            type="Medium"
            style={styles.textShowMore}
            onPress={toggleTextShown}
          />
        ) : null}
      </View>
    );
  };

  return (
    <>
      <View
        style={[
          Layouting().centered,
          Layouting(undefined, undefined, 'white').shadow,
          styles.container,
        ]}>
        <View
          style={[
            Layouting().flexRow,
            Layouting('flex-start').spaceBetween,
            styles.content,
          ]}>
          <RenderPoster />

          <View
            style={[
              Layouting(undefined, 2).flex,
              Layouting('flex-start').spaceBetween,
              styles.detailsContainer,
            ]}>
            <View style={[Layouting('flex-start').spaceAround]}>
              <RenderTitle />
              <RenderDateReview />
              <RenderRating />
            </View>

            <RenderIcons />
          </View>
        </View>

        <RenderReview />
      </View>
      <RenderModal />
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: Size.wp92,
    marginVertical: Size.ms10,
    paddingHorizontal: Size.wp4,
    paddingVertical: Size.hp1,
    borderRadius: Size.ms20,
    borderWidth: 1.2,
  },
  content: {
    paddingVertical: Size.hp1,
  },
  poster: {
    width: widthPercentageToDP(20),
    height: Size.hp20,
    position: 'relative',
  },
  detailsContainer: {
    height: Size.hp20,
    marginLeft: Size.ms12,
  },
  contentDetails: {
    marginVertical: moderateScale(1.5),
  },
  icons: {
    borderRadius: Size.ms40,
    width: Size.ms20,
    height: Size.ms20,
    backgroundColor: Color.blue,
    marginRight: Size.ms10,
  },
  textShowMore: {
    textAlign: 'right',
  },
  modal: {
    backgroundColor: 'transparent',
    zIndex: 10,
  },
});
