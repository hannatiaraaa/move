import React from 'react';
import {Header} from 'react-native-elements';

// component
import Roboto from '../../../shared/components/Roboto';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

export default function HeaderReview() {
  return (
    <Header
      containerStyle={[
        Layouting().spaceAround,
        {
          backgroundColor: Color.blue,
          height: Size.hp10,
        },
      ]}
      centerComponent={
        <Roboto
          title="Your Reviews"
          size={Size.ms20}
          color={Color.lightBlue}
          type="MediumItalic"
        />
      }
    />
  );
}
