import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {StyleSheet, ScrollView} from 'react-native';

// component
import HeaderReview from './components/Header';
import CardItem from './components/CardItem';

// global
import {Layouting} from '../../shared/global/Layouting';
import {Size} from '../../shared/global/config';

export default function Review() {
  return (
    <>
      <HeaderReview />
      <SafeAreaView
        style={[Layouting().flex, Layouting().centered, styles.container]}>
        <ScrollView>
          <CardItem />
          <CardItem />
          <CardItem />
        </ScrollView>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
  },
});
