import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {FlatList, StyleSheet} from 'react-native';

// component
import ReviewItem from './components/ReviewItem';

// global
import {Layouting} from '../../shared/global/Layouting';
import {Size} from '../../shared/global/config';

// redux
import {connect} from 'react-redux';
import {getReviews} from './redux/action';

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

function AllReviews(props) {
  const [refreshing, setRefreshing] = useState(false);

  const onRefresh = () => {
    setRefreshing(true);

    wait(1000).then(() => {
      setRefreshing(false);
    });
  };

  useEffect(() => {
    props.getReviews(props.route.params);
  }, []);

  return (
    <SafeAreaView style={[Layouting().flex, styles.container]}>
      <FlatList
        onRefresh={onRefresh}
        refreshing={refreshing}
        keyExtractor={(item) => item.id.toString()}
        data={props.listReviews}
        renderItem={({item}) => {
          return <ReviewItem review={item} />;
        }}
      />
    </SafeAreaView>
  );
}

const mapStateToProps = (state) => {
  return {
    listReviews: state.HomeReducer.listReviews,
  };
};

const mapDispatchToProps = {
  getReviews,
};

export default connect(mapStateToProps, mapDispatchToProps)(AllReviews);

const styles = StyleSheet.create({
  container: {
    paddingTop: Size.hp2,
    paddingHorizontal: Size.wp4,
  },
});
