import {all, call, put, takeLatest} from 'redux-saga/effects';

// api
import {request, MOVIE_URL, ENDPOINT_MOVIE} from '../../../shared/utils/api';
import {GENRE_URL, DISCOVER_URL, REVIEW_URL} from '../endPoint';

// action
import {
  GET_GENRES,
  setGenres,
  GET_DISCOVER_MOVIES,
  GET_MOVIES_FROM_GENRES,
  setMovies,
  GET_REVIEWS,
  setReviews,
} from './action';

function* getGenresSaga() {
  const res = yield call(request, `${MOVIE_URL}`, `${GENRE_URL}`, 'GET');

  if (res.status) {
    const data = res.data.genres;
    data.unshift({id: 'discover', name: 'Discover'});

    yield put(setGenres(data));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getMoviesSaga() {
  const res = yield call(request, `${MOVIE_URL}`, `${DISCOVER_URL}`, 'GET');

  if (res.status) {
    yield put(setMovies(res.data.results));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getMoviesFromGenresSaga({id}) {
  const res = yield call(
    request,
    `${MOVIE_URL}`,
    `${DISCOVER_URL}&with_genres=${id}`,
    'GET',
  );

  if (res.status) {
    yield put(setMovies(res.data.results));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getReviewsSaga({id}) {
  const res = yield call(
    request,
    `${MOVIE_URL}`,
    `${ENDPOINT_MOVIE}/${id}${REVIEW_URL}`,
    'GET',
  );

  if (res.status) {
    yield put(setReviews(res.data.results));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

export function* HomeSaga() {
  yield all([
    takeLatest(GET_GENRES, getGenresSaga),
    takeLatest(GET_DISCOVER_MOVIES, getMoviesSaga),
    takeLatest(GET_MOVIES_FROM_GENRES, getMoviesFromGenresSaga),
    takeLatest(GET_REVIEWS, getReviewsSaga),
  ]);
}
