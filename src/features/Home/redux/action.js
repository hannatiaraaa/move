// genres
export const GET_GENRES = 'GET_GENRES';
export const SET_GENRES = 'SET_GENRES';

// movies
export const GET_DISCOVER_MOVIES = 'GET_DISCOVER_MOVIES';
export const GET_MOVIES_FROM_GENRES = 'GET_MOVIES_FROM_GENRES';
export const SET_MOVIES = 'SET_MOVIES';

// reviews
export const GET_REVIEWS = 'GET_REVIEWS';
export const SET_REVIEWS = 'SET_REVIEWS';

// GET
export const getGenres = () => {
  return {
    type: GET_GENRES,
  };
};

export const getDiscoverMovies = () => {
  return {
    type: GET_DISCOVER_MOVIES,
  };
};

export const getMoviesFromGenres = (id) => {
  return {
    type: GET_MOVIES_FROM_GENRES,
    id,
  };
};

export const getReviews = (id) => {
  return {
    type: GET_REVIEWS,
    id,
  };
};

// SET
export const setGenres = (payload) => {
  return {
    type: SET_GENRES,
    payload,
  };
};

export const setMovies = (payload) => {
  return {
    type: SET_MOVIES,
    payload,
  };
};

export const setReviews = (payload) => {
  return {
    type: SET_REVIEWS,
    payload,
  };
};
