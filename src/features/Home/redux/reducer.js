import {SET_GENRES, SET_MOVIES, SET_REVIEWS} from './action';

const initialState = {
  listGenres: [],
  listMovies: [],
  listMoviesFromGenres: [],
  listReviews: [],
};

export const HomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_GENRES:
      return {
        ...state,
        listGenres: action.payload,
      };

    case SET_MOVIES:
      return {
        ...state,
        listMovies: action.payload,
        listMoviesFromGenres: action.payload,
      };

    case SET_REVIEWS:
      return {
        ...state,
        listReviews: action.payload,
      };

    default:
      return state;
  }
};
