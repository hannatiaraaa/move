import {ENDPOINT_MOVIE} from '../../shared/utils/api';
import {API_KEY} from '../../shared/utils/key';

// query params
const adult = 'include_adult';
const video = 'include_video';

// query body
const EN = 'en-US';
const popular = 'popularity.desc';

export const GENRE_URL = `/genre${ENDPOINT_MOVIE}/list?api_key=${API_KEY}&language=${EN}`;
export const DISCOVER_URL = `/discover${ENDPOINT_MOVIE}?api_key=${API_KEY}&language=${EN}&sort_by=${popular}&${adult}=false&${video}=true&page=1`;
export const REVIEW_URL = `/reviews?api_key=${API_KEY}&language=${EN}&page=1`;
