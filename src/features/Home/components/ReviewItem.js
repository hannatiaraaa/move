import React, {useCallback, useEffect, useState} from 'react';
import {View, StyleSheet} from 'react-native';
import FastImage from 'react-native-fast-image';
import {AirbnbRating} from 'react-native-elements';

// component
import Roboto from '../../../shared/components/Roboto';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// icon
import Fontisto from 'react-native-vector-icons/Fontisto';

export default function ReviewItem({review}) {
  const [showMoreButton, setShowMoreButton] = useState(false);
  const [textShown, setTextShown] = useState(false);
  const [numLines, setNumLines] = useState(undefined);

  useEffect(() => {
    setNumLines(textShown ? undefined : 2);
  }, [textShown]);

  const toggleTextShown = () => {
    setTextShown(!textShown);
  };

  const onTextLayout = useCallback(
    (e) => {
      if (e.nativeEvent.lines.length > 2 && !textShown) {
        setShowMoreButton(true);
        setNumLines(2);
      }
    },
    [textShown],
  );

  const photo = review.author_details.avatar_path;

  const RenderAvatar = () => {
    return (
      <View style={[Layouting().flex, Layouting().centered]}>
        {photo === undefined || photo === null ? (
          <View style={[Layouting().centered, styles.avatar]}>
            <Fontisto name="person" size={Size.ms20} color={Color.paleBlue} />
          </View>
        ) : photo.substring(1, 5) === 'http' ? (
          <FastImage
            source={{
              uri: `${photo.substring(1)}`,
            }}
            style={[
              {
                ...StyleSheet.absoluteFillObject,
              },
              styles.avatar,
            ]}
          />
        ) : (
          <FastImage
            source={{
              uri: `https://image.tmdb.org/t/p/w500${photo}`,
            }}
            style={[
              {
                ...StyleSheet.absoluteFillObject,
              },
              styles.avatar,
            ]}
          />
        )}
      </View>
    );
  };

  const RenderRating = () => {
    return (
      <View style={[Layouting().centerAlign, Layouting().flex]}>
        <AirbnbRating
          count={1}
          showRating={false}
          isDisabled={true}
          defaultRating={review.author_details.rating}
          size={Size.ms16}
          fractions={1}
        />
        <Roboto
          title={
            review.author_details.rating === undefined ||
            review.author_details.rating === null ? (
              <Roboto title="No Rate" color="black" size={Size.ms10} />
            ) : (
              <>
                <Roboto
                  title={review.author_details.rating}
                  color="black"
                  type="Bold"
                />{' '}
                / 10
              </>
            )
          }
          color="black"
          size={Size.ms10}
        />
      </View>
    );
  };

  const RenderTitle = () => {
    return (
      <Roboto
        title="Review Headliness!!!"
        size={Size.ms20}
        type="Black"
        color="black"
      />
    );
  };

  const RenderUsername = () => {
    return (
      <Roboto
        title={
          <>
            Reviewer:{' '}
            {review.author_details.username !== undefined
              ? review.author_details.username
              : review.author_details.name !== undefined
              ? review.author_details.name
              : review.author}
          </>
        }
        size={Size.ms10}
        color="black"
      />
    );
  };

  const RenderReview = () => {
    return (
      <View style={styles.content}>
        <Roboto
          title={review.content}
          color="black"
          onTextLayout={onTextLayout}
          numberOfLines={numLines}
        />
        {showMoreButton ? (
          <Roboto
            title={textShown ? 'Read less...' : 'Read more...'}
            color={Color.blue}
            type="Medium"
            style={styles.textShowMore}
            onPress={toggleTextShown}
          />
        ) : null}
      </View>
    );
  };

  return (
    <View
      style={[
        Layouting().centered,
        Layouting(undefined, undefined, 'white').shadow,
        styles.container,
      ]}>
      <View style={[Layouting().flexRow, Layouting().centered, styles.content]}>
        <RenderAvatar />

        <View
          style={[
            Layouting().flexRow,
            Layouting().centered,
            Layouting(undefined, 6).flex,
          ]}>
          <RenderRating />

          <View
            style={[
              Layouting(undefined, 4).flex,
              Layouting('flex-start').centered,
            ]}>
            <RenderTitle />

            <RenderUsername />
          </View>
        </View>
      </View>

      <RenderReview />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: Size.wp92,
    marginVertical: Size.ms6,
    paddingHorizontal: Size.wp4,
    paddingVertical: Size.hp1,
    borderRadius: Size.ms20,
    borderWidth: 1.2,
  },
  content: {
    paddingVertical: Size.hp1,
  },
  avatar: {
    borderRadius: Size.ms80,
    width: Size.ms40,
    height: Size.ms40,
    position: 'relative',
    backgroundColor: Color.primaryDark,
  },
  textShowMore: {
    textAlign: 'right',
  },
});
