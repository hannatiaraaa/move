import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';

// component
import Roboto from '../../../shared/components/Roboto';

// global
import {Layouting} from '../../../shared/global/Layouting';
import {Color, Size} from '../../../shared/global/config';

export default function GenreItem({genre, onPress}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.8}
      style={[
        Layouting().centered,
        Layouting(undefined, undefined, 'white').shadow,
        styles.container,
      ]}>
      <Roboto
        title={genre.name}
        type="Medium"
        color="white"
        style={Layouting().centerAlign}
      />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.blue,
    marginHorizontal: Size.ms12,
    paddingHorizontal: Size.wp4,
    paddingVertical: Size.hp1,
    height: Size.ms32,
    borderRadius: Size.ms6,
    borderColor: Color.lightBlue,
    borderWidth: 1.2,
  },
});
