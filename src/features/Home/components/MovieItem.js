import React, {useCallback, useEffect, useState} from 'react';
import {Alert, StyleSheet, View, TouchableHighlight} from 'react-native';
import FastImage from 'react-native-fast-image';
import {moderateScale} from 'react-native-size-matters';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import {AirbnbRating, Card, Overlay} from 'react-native-elements';

// component
import Roboto from '../../../shared/components/Roboto';
import {ModalCard} from '../../../shared/components/ModalCard';

// global
import {Layouting} from '../../../shared/global/Layouting';
import {Color, Size} from '../../../shared/global/config';

// icon
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export default function MovieItem({
  movie,
  onPressActive,
  onChangeScreen,
  active = false,
  currentId,
  itemId,
}) {
  const [showMoreButton, setShowMoreButton] = useState(false);
  const [textShown, setTextShown] = useState(false);
  const [numLines, setNumLines] = useState(undefined);
  const [modalVisible, setModalVisible] = useState(false);
  const [rating, setRating] = useState(0);

  useEffect(() => {
    setNumLines(textShown ? 22 : 8);
  }, [textShown]);

  const toggleTextShown = () => {
    setTextShown(!textShown);
  };

  const onTextLayout = useCallback(
    (e) => {
      if (e.nativeEvent.lines.length > 8 && !textShown) {
        setShowMoreButton(true);
        setNumLines(8);
      }
    },
    [textShown],
  );

  const RenderBackdrop = () => {
    return (
      <FastImage
        source={{
          uri: `https://image.tmdb.org/t/p/w500${movie.backdrop_path}`,
        }}
        resizeMode="contain"
        style={[
          {
            ...StyleSheet.absoluteFillObject,
          },
          Layouting().shadow,
          styles.backdrop,
        ]}
      />
    );
  };

  const RenderTitle = () => {
    return (
      <Roboto
        title={
          movie.title !== undefined
            ? movie.title
            : movie.name !== undefined
            ? movie.name
            : movie.original_title !== undefined
            ? movie.original_title
            : movie.original_name
        }
        size={Size.ms20}
        color="black"
        type="Bold"
        numberOfLines={2}
        style={stylesCollapse.title}
      />
    );
  };

  const date = new Date(movie.release_date);
  const year = date.getFullYear().toString();

  const RenderYear = () => {
    return (
      <Roboto
        title={year}
        size={Size.ms16}
        color="black"
        type="Bold"
        style={stylesCollapse.rightText}
      />
    );
  };

  const RenderDivider = () => {
    return (
      <Card.Divider
        style={[Layouting().centered, styles.content, styles.divider]}
      />
    );
  };

  const RenderPoster = () => {
    return (
      <FastImage
        source={{
          uri: `https://image.tmdb.org/t/p/w500${movie.poster_path}`,
        }}
        resizeMode="contain"
        style={[
          {
            ...StyleSheet.absoluteFillObject,
          },
          Layouting().shadow,
          stylesCollapse.poster,
        ]}
      />
    );
  };

  const RenderModal = () => {
    return (
      <Overlay
        isVisible={modalVisible}
        animationType="slide"
        fullScreen={true}
        overlayStyle={{backgroundColor: 'transparent'}}
        onRequestClose={() => {
          Alert.alert('Your edited review should be submitted');
        }}>
        <ModalCard
          onPress={() => {
            setModalVisible(!modalVisible);
          }}
          startingValue={rating}
          onFinishRating={(text) => setRating(text)}
        />
      </Overlay>
    );
  };

  const RenderRating = () => {
    return (
      <View style={Layouting().centerAlign}>
        <AirbnbRating
          count={1}
          showRating={false}
          isDisabled={true}
          defaultRating={movie.vote_average}
          size={Size.ms20}
          fractions={1}
        />
        <Roboto
          title={
            <>
              <Roboto
                title={movie.vote_average}
                size={Size.ms16}
                color="black"
                type="Bold"
              />{' '}
              / 10
            </>
          }
          color="black"
        />
      </View>
    );
  };

  const RenderCreateRating = () => {
    return (
      <View style={Layouting().centerAlign}>
        <TouchableHighlight
          onPress={() => {
            setModalVisible(true);
          }}
          underlayColor="transparent">
          <AirbnbRating
            count={1}
            showRating={false}
            isDisabled={true}
            defaultRating={0}
            size={Size.ms20}
            fractions={1}
          />
        </TouchableHighlight>
        <Roboto title="Rate this" color="black" size={Size.ms14} />
      </View>
    );
  };

  const RenderOverviewCollapse = () => {
    return (
      <>
        <Roboto
          title={movie.overview}
          color="black"
          onTextLayout={onTextLayout}
          numberOfLines={numLines}
        />
        {showMoreButton ? (
          <Roboto
            title={textShown ? 'Read less...' : 'Read more...'}
            color="gray"
            size={Size.ms10}
            type="Medium"
            style={Layouting().centerAlign}
            onPress={toggleTextShown}
          />
        ) : null}
      </>
    );
  };

  const RenderIcons = () => {
    return (
      <View
        style={[
          Layouting().flexRow,
          Layouting().spaceBetween,
          styles.iconsContainer,
        ]}>
        <FontAwesome
          name="comment"
          color={Color.blue}
          size={Size.ms16}
          onPress={onChangeScreen}
        />
        <FontAwesome
          name={
            active && currentId === itemId
              ? 'chevron-circle-up'
              : 'chevron-circle-down'
          }
          color={Color.blue}
          size={Size.ms20}
          onPress={onPressActive}
        />
        <FontAwesome name="share-alt" color={Color.blue} size={Size.ms16} />
      </View>
    );
  };

  return (
    <Card
      containerStyle={[
        Layouting().centered,
        Layouting(undefined, undefined, 'white').shadow,
        styles.container,
        {
          height:
            active && currentId === itemId
              ? textShown
                ? Size.hp100
                : Size.hp72
              : Size.hp48,
        },
      ]}>
      <View
        style={[
          textShown
            ? Layouting(undefined, 2).flex
            : Layouting(undefined, 4).flex,
          Layouting().centered,
          styles.content,
        ]}>
        <RenderBackdrop />
      </View>

      {active && currentId === itemId ? (
        <View
          style={[
            Layouting(undefined, 6).flex,
            Layouting().spaceBetween,
            styles.content,
          ]}>
          <View
            style={[
              textShown ? Layouting(undefined, 0.5).flex : Layouting().flex,
              Layouting().flexRow,
              Layouting().spaceBetween,
            ]}>
            <RenderTitle />
            <RenderYear />
          </View>

          <View
            style={[
              Layouting(undefined, 4).flex,
              stylesCollapse.detailsContainer,
            ]}>
            <RenderDivider />

            <View style={[Layouting().flexRow, Layouting().spaceBetween]}>
              <RenderPoster />

              <View style={[Layouting().spaceBetween, stylesCollapse.details]}>
                <View
                  style={[
                    Layouting().flexRow,
                    Layouting().spaceAround,
                    stylesCollapse.details,
                  ]}>
                  <RenderRating />
                  <RenderCreateRating />
                  <RenderModal />
                </View>

                <RenderOverviewCollapse />
              </View>
            </View>
          </View>
        </View>
      ) : (
        <View
          style={[
            Layouting(undefined, 2).flex,
            Layouting().centered,
            styles.content,
          ]}>
          <Roboto title={movie.overview} color="black" numberOfLines={5} />
        </View>
      )}

      <View
        style={[
          textShown ? Layouting(undefined, 0.5).flex : Layouting().flex,
          styles.content,
        ]}>
        <RenderDivider />
        <RenderIcons />
      </View>
    </Card>
  );
}

const styles = StyleSheet.create({
  container: {
    marginBottom: Size.ms100,
    width: Size.wp92,
    borderRadius: Size.ms20,
    backgroundColor: Color.lightBlue,
    paddingVertical: Size.hp1,
  },
  content: {
    width: Size.wp80,
    marginVertical: Size.hp1,
  },
  backdrop: {
    width: Size.wp80,
    height: moderateScale(168),
  },
  divider: {
    height: moderateScale(1),
    borderColor: 'gray',
  },
  overview: {
    paddingTop: Size.hp4,
  },
  iconsContainer: {
    paddingHorizontal: Size.wp4,
  },
});

const stylesCollapse = StyleSheet.create({
  row: {
    width: widthPercentageToDP(40),
  },
  title: {
    width: widthPercentageToDP(60),
  },
  rightText: {
    width: widthPercentageToDP(20),
    textAlign: 'right',
  },
  poster: {
    width: widthPercentageToDP(28),
    height: Size.hp20,
    position: 'relative',
  },
  detailsContainer: {
    width: Size.wp80,
    height: Size.hp20,
  },
  details: {
    width: widthPercentageToDP(50),
    paddingLeft: Size.ms6,
    marginBottom: Size.hp1,
  },
});
