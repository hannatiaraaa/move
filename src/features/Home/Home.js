import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {BackHandler, FlatList, StyleSheet, View} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import {SearchBar} from 'react-native-elements';

// component
import GenreItem from './components/GenreItem';
import MovieItem from './components/MovieItem';
import Roboto from '../../shared/components/Roboto';

// global
import {Layouting} from '../../shared/global/Layouting';
import {Color, Size} from '../../shared/global/config';

// icon
import Ionicons from 'react-native-vector-icons/Ionicons';

// redux
import {connect} from 'react-redux';
import {
  getGenres,
  getDiscoverMovies,
  getMoviesFromGenres,
} from './redux/action';

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

function Home(props) {
  const [id, setId] = useState(0);
  const [genreName, setGenreName] = useState('Discover');
  const [refreshing, setRefreshing] = useState(false);
  const [active, setActive] = useState(false);
  const [search, setSearch] = useState('');

  useEffect(() => {
    props.getGenres();
    props.getDiscoverMovies();
  }, []);

  BackHandler.addEventListener('hardwareBackPress', function () {
    BackHandler.exitApp();
  });

  const onRefresh = () => {
    setRefreshing(true);

    wait(1).then(() => {
      setRefreshing(false);
    });
  };

  const collapse = (id) => {
    if (active) {
      setActive(false);
      setId(0);
    } else {
      setActive(true);
      setId(id);
    }
  };

  const displayListMovies = (id, name) => {
    if (id === 'discover') {
      props.getDiscoverMovies();
    } else {
      props.getMoviesFromGenres(id);
    }
    setGenreName(name);
  };

  const navigateToReviews = (id) => {
    props.navigation.navigate('AllReviews', id);
  };

  return (
    <SafeAreaView style={[Layouting().flex, styles.container]}>
      <SearchBar
        placeholder="Search movies"
        round
        onChangeText={(text) => setSearch(text)}
        searchIcon={
          <Ionicons name="md-search" color={Color.blue} size={Size.ms20} />
        }
        clearIcon={
          <Ionicons name="close-circle" color={Color.blue} size={Size.ms20} />
        }
        containerStyle={[styles.horizontalContainer, styles.searchbarContainer]}
        inputContainerStyle={[styles.searchbar]}
      />
      {active ? null : (
        <>
          <View style={styles.genreContainer}>
            <View style={[styles.horizontalContainer, styles.contentContainer]}>
              <Roboto title="Best Genre" size={Size.ms20} type="Black" />
            </View>

            <FlatList
              onRefresh={onRefresh}
              refreshing={refreshing}
              horizontal={true}
              keyExtractor={(item) => item.id.toString()}
              data={props.listGenres}
              renderItem={({item}) => {
                return (
                  <GenreItem
                    genre={item}
                    onPress={() => displayListMovies(item.id, item.name)}
                    name={genreName}
                  />
                );
              }}
            />
          </View>
          <View style={[styles.horizontalContainer, styles.contentContainer]}>
            <Roboto
              title={<>Hot {genreName} Movies </>}
              size={Size.ms20}
              type="Black"
            />
          </View>
        </>
      )}

      <FlatList
        onRefresh={onRefresh}
        refreshing={refreshing}
        keyExtractor={(item) => item.id.toString()}
        data={props.listMovies}
        renderItem={({item}) => {
          return (
            <MovieItem
              movie={item}
              onPressActive={() => collapse(item.id)}
              currentId={item.id}
              itemId={id}
              active={active}
              onChangeScreen={() => navigateToReviews(item.id)}
            />
          );
        }}
      />
    </SafeAreaView>
  );
}

const mapStateToProps = (state) => {
  return {
    listGenres: state.HomeReducer.listGenres,
    listMovies: state.HomeReducer.listMovies,
    listMoviesFromGenres: state.HomeReducer.listMoviesFromGenres,
  };
};

const mapDispatchToProps = {
  getGenres,
  getDiscoverMovies,
  getMoviesFromGenres,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

const styles = StyleSheet.create({
  container: {
    paddingTop: Size.hp4,
  },
  horizontalContainer: {
    paddingHorizontal: Size.wp4,
  },
  contentContainer: {
    marginVertical: Size.ms10,
  },
  searchbarContainer: {
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
  },
  searchbar: {
    width: Size.wp92,
    height: Size.ms40,
    borderRadius: Size.ms10,
    backgroundColor: Color.paleBlue,
  },
  genreContainer: {
    height: heightPercentageToDP(12),
  },
});
