import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {View, ScrollView, StyleSheet} from 'react-native';
import {Input} from 'react-native-elements';
import {moderateScale} from 'react-native-size-matters';

// component
import {BlueButton} from '../../shared/components/BlueButton';
import Roboto from '../../shared/components/Roboto';

// global
import {Layouting} from '../../shared/global/Layouting';
import {Color, Size} from '../../shared/global/config';

// icon
import Fontisto from 'react-native-vector-icons/Fontisto';
import Zocial from 'react-native-vector-icons/Zocial';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

// redux
import {connect} from 'react-redux';
import {actionRegister} from './redux/action';

function Register(props) {
  const [disabled, setDisabled] = useState(true);
  const [name, setName] = useState('');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [focused, setFocused] = useState(true);

  useEffect(() => {
    handleFocus();
    handleBlur();
  }, []);

  const handleFocus = () => {
    setFocused(true);
  };

  const handleBlur = () => {
    setFocused(false);
  };

  const validateEmail = (text) => {
    setEmail(text);
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email && regex.test(text)) {
      setEmailError(null);
      if (username && email && password) {
        setDisabled(false);
      }
    } else {
      setDisabled(true);
      setEmailError('Please enter a valid email address');
    }
  };

  const validatePassword = (text) => {
    setPassword(text);
    if (password.length >= 6) {
      setPasswordError(null);
      if (username && email && password) {
        setDisabled(false);
      }
    } else {
      setDisabled(true);
      setPasswordError('Passwords must be at least 6 characters long');
    }
  };

  const validateName = (text) => {
    if (name === undefined || name === null) {
      setName('');
    } else {
      setName(text);
    }

    if (username.length && email && password) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  };

  const validateUsername = (text) => {
    setUsername(text);
    if (username.length && email && password) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  };

  const RegisterProcess = () => {
    props.actionRegister({
      name,
      username,
      email,
      password,
    });
  };

  const navigateToLogin = () => {
    props.navigation.navigate('Login');
  };

  return (
    <SafeAreaView style={[Layouting().flex, Layouting().flexEnd]}>
      <ScrollView
        contentContainerStyle={[
          Layouting().flex,
          Layouting().centered,
          styles.container,
        ]}>
        <View style={[Layouting().centered, styles.icon]}>
          <Fontisto name="person" size={moderateScale(50)} color="black" />
        </View>

        <Input
          onFocus={handleFocus}
          onBlur={handleBlur}
          placeholder="Full Name"
          onChangeText={(text) => {
            validateName(text);
          }}
          autoCapitalize="words"
          inputContainerStyle={
            focused ? styles.borderInput : styles.borderNonInput
          }
          inputStyle={styles.textInput}
        />
        <Input
          onFocus={handleFocus}
          onBlur={handleBlur}
          placeholder="Username"
          autoCapitalize="none"
          onChangeText={(text) => {
            validateUsername(text);
          }}
          errorStyle={styles.textError}
          errorMessage={
            focused && !username.trim() ? 'This column is required' : null
          }
          inputContainerStyle={
            focused ? styles.borderInput : styles.borderNonInput
          }
          inputStyle={styles.textInput}
        />
        <Input
          onFocus={handleFocus}
          onBlur={handleBlur}
          placeholder="Email Address"
          autoCapitalize="none"
          leftIcon={
            <Zocial name="email" size={Size.ms20} color={Color.paleBlue} />
          }
          onChangeText={(text) => {
            validateEmail(text);
          }}
          errorStyle={styles.textError}
          errorMessage={focused ? emailError : null}
          inputContainerStyle={
            focused ? styles.borderInput : styles.borderNonInput
          }
          inputStyle={styles.textInput}
        />
        <Input
          onFocus={handleFocus}
          onBlur={handleBlur}
          placeholder="Password"
          autoCapitalize="none"
          leftIcon={
            <MaterialIcons
              name="lock"
              size={Size.ms20}
              color={Color.paleBlue}
            />
          }
          secureTextEntry={true}
          onChangeText={(text) => {
            validatePassword(text);
          }}
          errorStyle={styles.textError}
          errorMessage={focused ? passwordError : null}
          inputContainerStyle={
            focused ? styles.borderInput : styles.borderNonInput
          }
          inputStyle={styles.textInput}
        />

        <BlueButton
          onPress={RegisterProcess}
          title="sign up"
          disabled={disabled}
        />

        <Roboto
          title={
            <>
              Already have an account?{' '}
              <Roboto title="Sign In" onPress={navigateToLogin} type="Medium" />
            </>
          }
        />
      </ScrollView>
    </SafeAreaView>
  );
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = {
  actionRegister,
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);

const styles = StyleSheet.create({
  container: {
    width: Size.wp92,
    paddingHorizontal: Size.wp4,
  },
  icon: {
    borderRadius: moderateScale(50),
    width: Size.ms100,
    height: Size.ms100,
    backgroundColor: Color.paleBlue,
    margin: Size.ms12,
  },
  textInput: {
    color: 'white',
    fontSize: Size.ms16,
  },
  textError: {
    color: 'red',
  },
  borderInput: {
    borderColor: Color.blue,
  },
  borderNonInput: {
    borderColor: Color.paleBlue,
  },
});
