import {REGISTER} from './action';

const initialState = {
  name: '',
  username: '',
  email: '',
};

export const RegisterReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER:
      return {
        ...state,
        name: action.name,
        username: action.username,
        email: action.email,
      };

    default:
      return state;
  }
};
