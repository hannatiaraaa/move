export const REGISTER = 'REGISTER';

export const actionRegister = (payload) => {
  return {
    type: REGISTER,
    name: payload.name,
    username: payload.username,
    email: payload.email,
    password: payload.password,
  };
};
