import {Alert} from 'react-native';
import {all, call, put, takeLatest} from 'redux-saga/effects';

// api
import {request, AUTH_URL} from '../../../shared/utils/api';
import {REGISTER_URL} from '../endPoint';

// router
import {navigate} from '../../../router/navigate';

// action
import {REGISTER} from './action';
import {setToken} from '../../Login/redux/action';

function* actionRegisterSaga({name, username, email, password}) {
  const res = yield call(request, `${AUTH_URL}`, `${REGISTER_URL}`, 'POST', {
    body: {
      name,
      username,
      email,
      password,
    },
  });

  if (res.status) {
    yield put(setToken(res.data.token));
    yield navigate({
      name: 'LoggedIn',
      params: {},
    });
  } else {
    Alert.alert(undefined, 'This email address is on another account', [
      {
        text: 'Log In',
        style: 'cancel',
        onPress: () =>
          navigate({
            name: 'Login',
            params: {},
          }),
      },
      {text: 'Try Again', style: 'default'},
    ]);
  }
}

export function* RegisterSaga() {
  yield all([takeLatest(REGISTER, actionRegisterSaga)]);
}
