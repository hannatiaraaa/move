<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/binar-10-mini-project/team-e/team-e-react-native/move">
    <img src="src/assets/images/logo.png" alt="Logo" width="100" height="100">
  </a>

  <h3 align="center">movE - mobile application</h3>

  <p align="center">
    Team E (Mini Project Batch #10)
    <br />
    <a href="https://gitlab.com/binar-10-mini-project"><strong>Glints Academy with Binar</strong></a>
    <br />
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#showcase">Showcase</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
    <li><a href="#copyright">Copyright</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

A simple movie application for mobile that we can find interesting informations and reviews relating to the popular movie based on its genre. This miniproject is the result of the team work of Team E whom we collaborated for 1 week to finish as much as we could.

### Built With

- [React Native](https://reactnative.dev)

<!-- GETTING STARTED -->

## Getting Started

After the clone process has been done, you have to install a few things before this app can run on your local machine. You can choose the following methods:

### Prerequisites

You need to have yarn or npm that has been installed in your computer. This is how to install them:

- npm

  ```sh
  npm install npm@latest -g
  ```

- yarn

  Once you have npm installed you can run the following both to install and upgrade Yarn:
  ```sh
  npm install --global yarn
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/binar-10-mini-project/team-e/team-e-react-native/move
   ```
2. Install dependencies
   ```sh
   yarn install
   ```
   or
   ```sh
   npm install
   ```

<!-- SHOWCASE -->

## Showcase

<a href="https://gitlab.com/binar-10-mini-project/team-e/team-e-react-native/move">
    <img src="src/assets/images/ScreenShot1.png" alt="Showcase" width="1264" height="2220">
    <img src="src/assets/images/ScreenShot2.png" alt="Showcase" width="1264" height="2220">
</a>
  
<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/YourFeature`)
3. Commit your Changes (`git commit -m 'Add some YourFeature'`)
4. Push to the Branch (`git push origin feature/YourFeature`)
5. Open a Pull Request

<!-- CONTACT -->

## Contact

Hanna - hannatiara@gmail.com

Project Link: [https://gitlab.com/binar-10-mini-project/team-e/team-e-react-native/move](https://gitlab.com/binar-10-mini-project/team-e/team-e-react-native/move)

<!-- ACKNOWLEDGEMENTS -->

## Acknowledgements

- [TMDB API Documentation](https://developers.themoviedb.org/3)
- [React Navigation](https://reactnavigation.org/)
- [React Native Elements](https://reactnativeelements.com/)
- [Redux Saga](https://redux-saga.js.org/)
- [Axios](https://github.com/axios/axios)
- [React Native Vector Icons](https://github.com/oblador/react-native-vector-icons)
- [React Native Fast Image](https://github.com/DylanVann/react-native-fast-image)
- [React Native Splash Screen](https://github.com/crazycodeboy/react-native-splash-screen)

<!-- COPYRIGHT -->

## Copyright

© Copyright by [Hanna Tiara Andarlia](https://gitlab.com/hannatiaraaa/ 'hannatiaraaa')
